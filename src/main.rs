#![warn(clippy::all, clippy::pedantic)]
#![allow(dead_code)]
#[macro_use] extern crate log;
use std::path::{PathBuf, Path};
use std::io;
use std::fs::{self, DirEntry};
use git2::Repository;
use url::{Url, ParseError};

// let url = "https://github.com/alexcrichton/git2-rs";
// let repo = match Repository::clone(url, "/path/to/a/repo") {
//     Ok(repo) => repo,
//     Err(e) => panic!("failed to clone: {}", e),
// };

fn _extract_url(url: &Url) -> Option<PathBuf> {
    if let Some(host) = url.host_str() {
        let path = url.path();
        if path.starts_with('/') {
            Some(Path::new(host).join(path.split_at(1).1))
        } else {
            Some(Path::new(host).join(path))
        }
    } else {
        error!("Cannot parse host in url={}", url);
        None
    }
}

fn parse_git_url(url: &str) -> Option<PathBuf> {
    println!("url={:?}", url);
    let url = str::replace(&url, ".git", "");
    let url = str::replace(&url, "git@", "");
    let url = str::replace(&url, '~', "");
    let (s,a) = url.split("://").collect();

    let url = str::replace(&url, ":", "/");
    println!("cleaned={:?}", url);
    match Url::parse(&url) {
        Ok(u) => _extract_url(&u),
        Err(e) => {
            if url.starts_with("git@") {
                debug!("Parsing url={}", url);
                // hacky
                let url = str::replace(&url, ":", "/");
                // let url = str::replace(&url, "git@", "fake://");
                let url = str::replace(&url, ".git", "");
                parse_git_url(&url)
            } else {
                error!("Cannot parse host in url={} err={}", url, e);
                None
            }
        },
    }
}

fn _visit_dirs(dir: &Path, repos: &mut Vec<PathBuf>) -> io::Result<()> {
    if dir.is_dir() {
        for entry in fs::read_dir(dir)? {
            let entry = entry?;
            let mut path = entry.path();
            if path.is_dir() {
                _visit_dirs(path.as_path(), repos)?;
            }
            if is_dot_git(&entry) {
                path.pop(); // remove the `/.git` ending
                repos.push(path);
            }
        }
    }
    Ok(())
}

fn visit_dirs(dir: &Path) -> io::Result<(Vec<PathBuf>)> {
    let mut git_repos = Vec::new();
    match _visit_dirs(dir, &mut git_repos) {
        Ok(_) => Ok(git_repos),
        Err(e) => Err(e),
    }
}

fn is_dot_git(dir: &DirEntry) -> bool {
    if let Ok(file_type) = dir.file_type() {
        return file_type.is_dir() && dir.file_name() == ".git"
    } else {
        error!("Unable to get filetype");
        return false;
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    macro_rules! git_parse_tests {
        ($($name:ident: $value:expr,)*) => {
        $(
            #[test]
            fn $name() {
                let (input, expected) = $value;
                assert_eq!(Some(PathBuf::from(expected)), parse_git_url(input));
            }
        )*
        }
    }

    git_parse_tests! {
        git_parse_01: ("ssh://git@example.com:user/repo.git", "example.com/user/repo"),
        git_parse_02: ("http://example.com/user/repo.git", "example.com/user/repo"),
        git_parse_03: ("ssh://git@git.example.com:7999/~user/repo.git", "git.example.com/user/repo"),
        git_parse_04: ("http://example.com/user/repo.git", "example.com/user/repo"),
        git_parse_05: ("https://user@git.example.com/scm/~user/repo.git", "git.example.com/user/repo"),
        git_parse_06: ("ftps://host.xz:9999/path/to/repo.git/", "host.xz/path/to/repo"),
        git_parse_07: ("ssh_host:chip8.git", "ssh_host/chip8"),
        git_parse_08: ("user@host.xz:/~user/path/to/repo.git/", "host.xz/user/path/to/repo"),
        git_parse_09: ("file:///path/to/repo.git/", "/path/to/repo"),
        git_parse_10: ("file:///path/to/repo/", "/path/to/repo"),
        git_parse_11: ("/home/absolute/path/to/myproject", "/home/absolute/path/to/myproject"),
        git_parse_12: ("http://example.com/user/repo", "example.com/user/repo"),
    }

    // #[test]
    // fn test_parse_git_url() {
    //     assert_eq!(git_path, Some(PathBuf::from("github.com/alexcrichton/git2-rs")));
    // }
}

fn main() {
    let url = "https://github.com/alexcrichton/git2-rs";
    println!("{:?}", parse_git_url(url));

    let url = "git@gitlab.com:megacrit/SlayTheSpire.git";
    println!("{:?}", parse_git_url(url));

    // if let Ok(git_repos) = visit_dirs(Path::new("mock")) {
        // println!("size={}", git_repos.len());
        // let _ = git_repos.iter().map(|x| println!("{:?}", x)).collect::<Vec<_>>();
    // }

    // # Feature 1: Clone
    // 1. Clone repo to path based on origin

    // # Feature 2: Reorganize
    // 1. find all git repos
    // 2. Extract origins of all git repos
    // 3. Offer to move repos to the path of their origins

    // # Feature 3: Notify of all files not in a repo and not organized.

    // # Feature 4: Call git fetch or other git commands over all repos
    println!("Done!");
}
